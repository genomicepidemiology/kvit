#!/bin/bash 
KVIT.py -i /test/test.fsa -o /test/ -s Arenaviridae -mp blastn -x --quiet
file=/test/results_tab.tsv
DIFF=$(diff $file /test/test_fsa_results.tsv)
if [ "$DIFF" == "" ] && [ -s $file ] ;
   then     
   echo "TEST SUCCEEDED"; 
else
   echo "TEST FAILED";
fi
