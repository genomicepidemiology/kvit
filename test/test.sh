#!/bin/bash 
kma_index -i /database/Arenaviridae/Arenaviridae.fsa -o /database/Arenaviridae/Arenaviridae &> /database/Arenaviridae/Arenaviridae.log
KVIT.py -i /test/test.fq.gz -o /test/ -s Arenaviridae -mp kma -x --quiet
file=/test/results_tab.tsv
DIFF=$(diff $file /test/test_fq_results.tsv)
if [ "$DIFF" == "" ] && [ -s $file ] ;
   then     
   echo "TEST SUCCEEDED"; 
else
   echo "TEST FAILED";
fi
