#!/usr/bin/env python3

################################################################################
# Import libraries
################################################################################
import sys
import os
from cgecore.blaster.blaster import Blaster
from cgecore.cgefinder import CGEFinder
#from blaster import Blaster
#from cgefinder import CGEFinder
import pprint
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
import csv
import subprocess
import shutil
import datetime
import time
import re, json
import gzip
from subprocess import Popen, PIPE
import argparse
from sortedcontainers import SortedDict
import matplotlib
matplotlib.use('pdf')
import matplotlib.pyplot as plt
from matplotlib import font_manager as fm
import seaborn as sns
from tabulate import tabulate

################################################################################
# FUNCTIONS:
################################################################################

def get_file_format(input_files):
    """
    Takes all input files and checks their first character to assess
    the file format. Returns one of the following strings; fasta, fastq,
    other or mixed. fasta and fastq indicates that all input files are
    of the same format, either fasta or fastq. other indiates that all
    files are not fasta nor fastq files. mixed indicates that the inputfiles
    are a mix of different file formats.
    """

    # Open all input files and get the first character
    file_format = []
    invalid_files = []
    for infile in input_files:
        if is_gzipped(infile):
            f = gzip.open(infile, "rb")
            fst_char = f.read(1);
        else:
            f = open(infile, "rb")
            fst_char = f.read(1);
        f.close()
        # Assess the first character
        if fst_char == b"@":
            file_format.append("fastq")
        elif fst_char == b">":
            file_format.append("fasta")
        else:
            invalid_files.append("other")
    if len(set(file_format)) != 1:
        return "mixed"
    return ",".join(set(file_format))

def is_gzipped(file_path):
    ''' Returns True if file is gzipped and False otherwise.
        The result is inferred from the first two bits in the file read
        from the input path.
        On unix systems this should be: 1f 8b
        Theoretically there could be exceptions to this test but it is
        unlikely and impossible if the input files are otherwise expected
        to be encoded in utf-8.
    '''
    with open(file_path, mode='rb') as fh:
        bit_start = fh.read(2)
    if(bit_start == b'\x1f\x8b'):
        return True
    else:
        return False

def get_tax_info(temp_hits, tax_info, tax_hits, fam_hits):
   with open(tax, "r") as t:
      for line in t:
         line = line.rstrip()
         hit = line.split("\t")[0]
         if hit in temp_hits:
            tmp = line.split("\t")
            if hit not in tax_info:
               tax_info[hit] = {}
            tax_info[hit]["org"] = tmp[1]
            tax_info[hit]["tax"] = tmp[2]
            tax_info[hit]["fam"] = tmp[3]
            if tmp[3] not in fam_hits:
               fam_hits[tmp[3]] = 0
            if tmp[1] not in tax_hits:
               tax_hits[tmp[1]] = 0
            fam_hits[tmp[3]] += temp_hits[hit]
            tax_hits[tmp[1]] += temp_hits[hit]

def my_autopct(pct):
    return ('%.1f%%' % pct) if pct > 2 else ''

def insert_newlines(string, every=20):
    return '\n'.join(string[i:i+every] for i in range(0, len(string), every))

def make_figures(out_folder, out_name, db_name, temp_hits, tax_hits, fam_hits):

   #sorted_temp = SortedDict(temp_hits)
   sorted_org = SortedDict(tax_hits)
   sorted_fam = SortedDict(fam_hits)

   #ax1.pie(list(sorted_temp.values()),autopct="%.1f%%", labels = list(sorted_temp.keys()))
   #ax1.set_title('Template level')

   fig = plt.figure(1, figsize=(10,9))
   sns.set_palette("Set2", len(sorted_org.keys()))
   plt.title('Distribution of aligned sequences at organism level',  fontsize=18)

   tmp_labels = [n if (v/sum(tax_hits.values()))*100 > 2  else ''
              for n, v in sorted_org.items()]

   labels = list()
   for label in tmp_labels:
      if (len(label)) > 22:
         l = insert_newlines(label, 22)
         labels.append(l)
      else:
         labels.append(label)

   patches, texts, autotexts = plt.pie(list(sorted_org.values()), autopct=my_autopct, labels = labels)
   proptease = fm.FontProperties()
   proptease.set_size(10)
   plt.setp(autotexts, fontproperties=proptease)
   plt.setp(texts, fontproperties=proptease)
   #plt.tight_layout()
   plt.savefig("%s/%s_%s_pie_chart_org.pdf"%(out_folder, out_name, db_name))
   plt.clf()

   fig = plt.figure(1, figsize=(10,9))
   sns.set_palette("Set2", len(sorted_fam.keys()))
   plt.title('Distribution of aligned sequences at family level',  fontsize=18)

   tmp_labels = [n if (v/sum(fam_hits.values()))*100 > 2  else ''
              for n, v in sorted_fam.items()]

   labels = list()
   for label in tmp_labels:
      if (len(label)) > 22:
         l = insert_newlines(label, 22)
         labels.append(l)
      else:
         labels.append(label)

   patches, texts, autotexts = plt.pie(list(sorted_fam.values()), autopct=my_autopct, labels = labels)
   proptease = fm.FontProperties()
   proptease.set_size(12)
   plt.setp(autotexts, fontproperties=proptease)
   plt.setp(texts, fontproperties=proptease)
   #plt.tight_layout()
   plt.savefig("%s/%s_%s_pie_chart_fam.pdf"%(out_folder, out_name, db_name))
   plt.clf()


def make_aln(species, file_handle, gene_matches, query_aligns, homol_aligns, sbjct_aligns):
    for gene_id, gene_info in gene_matches.items():
        hit_name = gene_matches[gene_id]["hit_name"]

        seqs = ["","",""]
        seqs[0] = sbjct_aligns[species][hit_name]
        seqs[1] = homol_aligns[species][hit_name]
        seqs[2] = query_aligns[species][hit_name]

        write_align(seqs, gene_id, file_handle)


def write_align(seq, seq_name, file_handle):
    file_handle.write("# {}".format(seq_name) + "\n")
    sbjct_seq = seq[0]
    homol_seq = seq[1]
    query_seq = seq[2]
    for i in range(0,len(sbjct_seq),60):
        file_handle.write("%-10s\t%s\n"%("template:", sbjct_seq[i:i+60]))
        file_handle.write("%-10s\t%s\n"%("", homol_seq[i:i+60]))
        file_handle.write("%-10s\t%s\n\n"%("query:", query_seq[i:i+60]))

def text_table(headers, rows, empty_replace='-'):
   ''' Create text table

   USAGE:
      >>> from tabulate import tabulate
      >>> headers = ['A','B']
      >>> rows = [[1,2],[3,4]]
      >>> print(text_table(headers, rows))
      **********
        A    B
      **********
        1    2
        3    4
      ==========
   '''
   # Replace empty cells with placeholder
   rows = map(lambda row: map(lambda x: x if x else empty_replace, row), rows)
   # Create table
   table = tabulate(rows, headers, tablefmt='simple').split('\n')
   # Prepare title injection
   width = len(table[0])
   # Switch horisontal line
   table[1] = '*'*(width+2)
   # Update table with title
   table = ("%s\n"*3)%('*'*(width+2), '\n'.join(table), '='*(width+2))
   return table



################################################################################
#  PARSE COMMANDLINE OPTIONS
################################################################################



start_time = time.time()

parser = argparse.ArgumentParser()

parser.add_argument('-i', '--input_file', nargs="+", required=True,  help="fasta or fastq input file")
parser.add_argument('-o', '--output_folder',  help="folder to store the output")
parser.add_argument('-db', '--db_path',  help="path to database", default = "/database")
parser.add_argument('-tax', '--tax',  help="file with additional data for each template in all databases (family, taxid and organism)")
parser.add_argument('-t', '--threshold',  help="minimum identity threshold for sequences matching entries in the database, default = 0.8", default = 0.8)
parser.add_argument('-l', '--coverage', help="minimum allignment coverage required, default = 0.0", default = 0.0)
parser.add_argument('-mp', '--method_path', help = "Full path to executable KMA or BLASTn program")
parser.add_argument('-s', '--species', help="Choose virus species, default is set to all available species", default='all_databases')
parser.add_argument("-x", "--extented_output", help="Give extented output with allignment files, template and query hits in fasta and" +
                                                     "a tab seperated file with allele profile results", action="store_true")
parser.add_argument("-q", "--quiet", action="store_true")
parser.add_argument("-np", "--nanopore", help="Specify if you reads are nanopore reads", action="store_true")

args = parser.parse_args()

if args.quiet:
   f = open('/dev/null', 'w')
   sys.stdout = f

# TODO take from config file
species=args.species
method_path = args.method_path
# Get input files
infile = []
for input in args.input_file:
   if os.path.isfile(input):
      infile.append(input)
   else:
      sys.stderr.write("Please specify a valid input file!\n{} does not exist.".format(args.input_file))
      sys.exit(2)

# Check output folder
if args.output_folder is None:
   outdir = os.getcwd()
elif os.path.isdir(args.output_folder):
   outdir = args.output_folder
else:
   sys.stderr.write("Problem with output folder (option -output_folder)!\n")
   sys.exit(2)

try:
   threshold = float(args.threshold)
except:
   sys.stderr.write("Problem with threshold (option -t)!\n")
   sys.exit(2)
try:
   min_cov = float(args.coverage)
except:
   sys.stderr.write("Problem with coverage (option -l)!\n")
   sys.exit(2)

# Get database path
if args.db_path != None and os.path.exists(args.db_path):
   db_path = args.db_path
else:
   sys.stderr.write("Please specify a database!\n")
   sys.exit(2)

# Check file with additional data (family, taxid and organism)
tax = False
if args.tax:
   tax = args.tax
else:
   if os.path.isfile("{}/database_overview.txt".format(outdir)):
      tax = "{}/database_overview.txt".format(outdir)

# Define outfile name
out_name = infile[0].split("/")[-1].split(".")[0]

# Keep treack of hits
fam_hits = {}
tax_hits = {}
temp_hits = {}
total_hits = 0
tax_info = {}

# Set db
db_name = species
db_path = "{}/{}/".format(db_path, species)

# Check file format (fasta, fastq or other format)
file_format = get_file_format(infile)

# Call appropriate method (kma or blastn) based on file format
if file_format == "fastq":
   if not method_path:
      method_path = "kma"
      if shutil.which(method_path) == None:
         sys.exit("No valid path to a kma program was provided. Use the -mp flag to provide the path.")
   # Check the number of files
   if len(infile) == 1:
      infile_1 = infile[0]
      infile_2 = None
   elif len(infile) == 2:
      infile_1 = infile[0]
      infile_2 = infile[1]
   else:
      sys.exit("Only 2 input file accepted for raw read data,\
                if data from more runs is avaliable for the same\
                sample, please concatinate the reads into two files")

   method = "kma"

   # Call KMA
   method_obj = CGEFinder.kma(infile_1, outdir, [species], db_path, min_cov=min_cov,
                              threshold=threshold, kma_path=method_path, sample_name=out_name,
                              inputfile_2=infile_2, kma_mrs=0.75, kma_gapopen=-5,
                              kma_gapextend=-1, kma_penalty=-3, kma_reward=1, kma_nanopore=args.nanopore,
                              kma_memmode=True)

elif file_format == "fasta":
   if not method_path:
       method_path = "blastn"
       if shutil.which(method_path) == None:
          sys.exit("No valid path to a blastn program was provided. Use the -mp flag to provide the path.")

   # Assert that only one fasta file is inputted
   assert len(infile) == 1, "Only one input file accepted for assembled data"
   infile = infile[0]
   method = "blast"

   # Call BLASTn
   method_obj = Blaster(infile, [species], db_path, outdir,
                        min_cov, threshold, method_path, cut_off=False)
else:
   sys.exit("Input file must be fastq or fasta format, not "+ file_format)

results      = method_obj.results
query_aligns = method_obj.gene_align_query
homol_aligns = method_obj.gene_align_homo
sbjct_aligns = method_obj.gene_align_sbjct

warning = ""
if results[species] == "No hit found":
   results[species] = {}
   warning = ("No virus hits were fould in the input data, "
              "make sure that the correct MLST scheme was chosen.")

# Look at the alignment of each fragment
if file_format == "fastq":
   with gzip.open("{}/kma_{}_{}.frag.gz".format(outdir, species, out_name), "rb") as frag:
      for line in frag:
         total_hits += 1
         line = line.decode().rstrip()
         tmp = line.split("\t")
         if len(tmp) != 7:
            continue
         temp = tmp[5].split(":")[0]
         aln_score = int(tmp[2])
         start = int(tmp[3])
         end = int(tmp[4])

         if aln_score/float((end - start)) >= 0.4:
            if temp not in temp_hits:
               temp_hits[temp] = 0
            temp_hits[temp] += 1
else:
   for hits in results[species].values():
      temp = hits.get("sbjct_header").strip().split(":")[0]
      if temp not in temp_hits:
         temp_hits[temp] = 0
      temp_hits[temp] += 1
      total_hits += 1

if tax:
   get_tax_info(temp_hits, tax_info, tax_hits, fam_hits)

   # Make output figures
#   make_figures(outdir, out_name, db_name, temp_hits, tax_hits, fam_hits)

   # Make family distribution overview text file
   with open("%s/%s_%s_families.tab"%(outdir, out_name, db_name), "w") as fam_txt:
      fam_txt.write("Family\tNumber of alignments\tPercentage of all alignments\n")
      for fam,no in sorted(fam_hits.items()):
         perc = (float(no)/int(total_hits)) * 100
         fam_txt.write("%s\t%s\t%.3f\n"%(fam, no, perc))

   # Make organism distribution overview text file
   with open("%s/%s_%s_organisms.tab"%(outdir, out_name, db_name), "w") as org_txt:
      org_txt.write("Organism\tNumber of alignments\tPercentage of all alignments\n")
      for org,no in sorted(tax_hits.items()):
         perc = (float(no)/int(total_hits)) * 100
         org_txt.write("%s\t%s\t%.3f\n"%(org, no, perc))


warning = ""
if results[species] == "No hit found":
   results[species] = {}
   warning = ("No virus hits were fould in the input data, "
              "make sure that the correct MLST scheme was chosen.")

gene_matches = {}

# Get the found gene profile contained in the results dict
for hit_id, gene_hit in results[species].items():

   # Get gene number for gene_hit
   sbj_name = gene_hit["sbjct_header"]

   # Get variable to later storage in the results dict
   gene_id   = sbj_name.split(":")[0]
   coverage  = float(gene_hit["perc_coverage"])
   identity  = float(gene_hit["perc_ident"])
   score     = float(gene_hit["cal_score"])
   gaps      = int(gene_hit["gaps"])
   align_len = gene_hit["HSP_length"]
   sbj_len   = int(gene_hit["sbjct_length"])
   sbjct_seq = gene_hit["sbjct_string"]
   query_seq = gene_hit["query_string"]
   homol_seq = gene_hit["homo_string"]
   try:
      depth = gene_hit["depth"]
      p_value = gene_hit["p_value"]
   except KeyError:
      depth = "-"
      p_value = "-"

   # Save hit in dict
   if float(coverage) >= (threshold * 100):
      gene_matches[gene_id] = {"template":sbj_name, "align_len":align_len, "sbj_len":sbj_len,
                               "score": score,"coverage":coverage, "identity":identity,
                               "gaps":gaps, "sbj_len":sbj_len, "p_value":p_value,
                               "sbjct_seq":sbjct_seq, "query_seq":query_seq, "homol_seq":homol_seq,
                               "depth":depth,"family":"unknown", "hit_name":hit_id,
                               "organism":"unknown", "taxonomy":"unknown"}
      if tax and gene_id in tax_info:
          gene_matches[gene_id]["family"]   = tax_info[gene_id]["fam"]
          gene_matches[gene_id]["organism"] = tax_info[gene_id]["org"]
          gene_matches[gene_id]["taxonomy"] = tax_info[gene_id]["tax"]

# Get run info for JSON file
service = os.path.basename(__file__).replace(".py", "")
date = time.strftime("%d.%m.%Y")
time_hms = time.strftime("%H:%M:%S")

# Make JSON output file
# TODO find a system to show the database and service version using git
data = {'service':service}
gene_results = {}
for gene_hit, gene_hit_info in gene_matches.items():
    gene_results[gene_hit] = {"identity":0, "coverage":0, "align_len":"", "gaps":0, "sbj_len":"", "template":"", "hit_name":"",
                              "family":"unknown", "organism":"unknown", "taxonomy":"unknown", "score":"-", "depth":"-" }
    for (key, value) in gene_hit_info.items():
        if key in gene_results[gene_hit]:
            gene_results[gene_hit][key] = value

data["user_input"] = {"filename":args.input_file, "species":args.species,"file_format":file_format}
data["run_info"] = {"date":date, "time":time_hms}#, "database":{"remote_db":remote_db, "last_commit_hash":head_hash}}
data["results"] = {"gene_profile": gene_results,
                   "notes":warning}

pprint.pprint(data)

# Save json output
result_file = "{}/data.json".format(outdir)
with open(result_file, "w") as outfile:
    json.dump(data, outfile)

if args.extented_output:
    # Define extented output
    table_filename  = "{}/results_tab.tsv".format(outdir)
    query_filename  = "{}/Hit_in_genome_seqs.fsa".format(outdir)
    sbjct_filename  = "{}/DB_seqs.fsa".format(outdir)
    result_filename = "{}/results.txt".format(outdir)
    table_file  = open(table_filename, "w")
    query_file  = open(query_filename, "w")
    sbjct_file  = open(sbjct_filename, "w")
    result_file = open(result_filename, "w")

    # Make results file
    result_file.write("{0} Results\n\n{0} Profile: {1}\n\n".format(service, species))

    # Write tsv table header
    table_header = ["Template accession no.", "Family", "Organism", "Taxid", "Identity", "Coverage", "Template length", "Alligenment length", "Depth", "Score"]
    table_file.write("\t".join(table_header) + "\n")
    rows = []
    for gene_hit, gene_info in gene_matches.items():
        gene_id  = gene_hit
        family   = str(gene_info["family"])
        organism = str(gene_info["organism"])
        taxonomy = str(gene_info["taxonomy"])
        identity = str(gene_info["identity"])
        coverage = str(gene_info["coverage"])
        sbj_len  = str(gene_info["sbj_len"])
        align_len = str(gene_info["align_len"])
        depth = str(gene_info["depth"])
        score = str(gene_info["score"])
        hit_name = str(gene_info["hit_name"])
        template = str(gene_info["template"])

        # Write gene results to tsv table
        row = [gene_id, family, organism, taxonomy, identity,
               coverage, sbj_len, align_len, depth, score]
        rows.append(row)

        # Write query fasta output

        query_seq = query_aligns[species][hit_name]
        sbjct_seq = sbjct_aligns[species][hit_name]
        homol_seq = homol_aligns[species][hit_name]

        header = ">{} ID:{}% COV:{}% BEST_MATCH:{}\n".format(hit_name, identity,
                                                  coverage, template)
        query_file.write(header)
        for i in range(0,len(query_seq),60):
            query_file.write(query_seq[i:i+60] + "\n")

        # Write template fasta output
        header = ">{}\n".format(template)
        sbjct_file.write(header)
        for i in range(0,len(sbjct_seq),60):
            sbjct_file.write(sbjct_seq[i:i+60] + "\n")

    # Write gene profile results tables in results file and table file
    rows.sort(key=lambda x: (float(x[4]), float(x[5]), float(x[-1])), reverse = True)

    #try:
    #   result_file.write("Family and Organism of best hit: {}, {}\n\n".format(rows[0][1], rows[0][2]))
    #except IndexError:
    #   pass
    for row in rows:
        table_file.write("\t".join(row) + "\n")
    if rows == []:
       content = ['']*len(table_header)
       content[int(len(table_header)/2)] = "No hit found"
       rows = [content]
    result_file.write(text_table(table_header, rows))

    # Write any notes
    if warning != "":
       result_file.write("\nNotes: {}\n\n".format(warning))

    # Write allignment output
    result_file.write("\n\nExtended Output:\n\n")
    make_aln(species, result_file, gene_matches, query_aligns, homol_aligns, sbjct_aligns)

    # Close all files
    query_file.close()
    sbjct_file.close()
    table_file.close()
    result_file.close()

if args.quiet:
    f.close()
