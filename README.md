===================
KVIT
===================

This project documents KVIT service


Documentation
=============

## What is it?

KVIT is a Kmer-based Viral Identification Tool, which utilizes KMA agains
a nucleotide based virus database, that can be downloaded seperatly,
as descibed in this documentation. The program outputs both text files
and graphs to illustrate the viral compositon of the input sample/samples.

## Content of the repository
1. KVTI.py      - the program
2. Dockerfile
3. test folder

## Installation

Setting up KVIT
```bash
# Go to wanted location for KVTI
cd /path/to/some/dir
# Clone and enter the KVTI directory
git clone https://bitbucket.org/genomicepidemiology/kvit.git
cd kvit
```

### Installing KVIT database:

# Clone from bitbucket
```
git clone https://bitbucket.org/genomicepidemiology/kvit_db.git
cd kvit_db
KVIT_DB=$(pwd)
```

# Install KVIT database with executable kma_index program
```
python3 INSTALL.py kma_index
```

If kma_index has not bin install please install kma_index from the kma repository: https://bitbucket.org/genomicepidemiology/kma
and add kma_index to your $PATH

## Usage

The program can be invoked with the -h option to get help and more information 
of the service. 

```bash
usage: KVIT.py [-h] -i INPUT_FILE [INPUT_FILE ...] [-o OUTPUT_FOLDER]
               [-db DB_PATH] [-tax TAX] [-t THRESHOLD] [-l COVERAGE]
               [-mp METHOD_PATH] [-s SPECIES] [-x] [-q]

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT_FILE [INPUT_FILE ...], --input_file INPUT_FILE [INPUT_FILE ...]
                        fasta or fastq input file
  -o OUTPUT_FOLDER, --output_folder OUTPUT_FOLDER
                        folder to store the output
  -db DB_PATH, --db_path DB_PATH
                        path to database
  -tax TAX, --tax TAX   file with additional data for each template in all
                        databases (family, taxid and organism)
  -t THRESHOLD, --threshold THRESHOLD
                        minimum identity threshold for sequences matching
                        entries in the database, default = 0.8
  -l COVERAGE, --coverage COVERAGE
                        minimum allignment coverage required, default = 0.6
  -mp METHOD_PATH, --method_path METHOD_PATH
                        Path to kma or blastn
  -s SPECIES, --species SPECIES
                        Choose virus species, default is set to all available
                        species
  -x, --extented_output
                        Give extented output with allignment files, template
                        and query hits in fasta anda tab seperated file with
                        allele profile results
  -q, --quiet

```

#### Build and test KVIT in docker

```bash
# Build kvit docker container
docker build -t kvit .

# Test running fastq files with KMA
docker run --rm -it \
           --entrypoint=/test/test.sh kvit

# Test running fasta files with BLASTn
docker run --rm -it \
           --entrypoint=/test/test_fsa.sh kvit

```

#### Run KVIT in docker
```
docker run --rm -it \
       -v $KVIT_DB:/database \
       -v $(pwd):/workdir \
       kvit -i [INPUTFILE] -o . -s [SPECIES] [-x]
```
## Web-server

A webserver implementing the methods (https://cge.cbs.dtu.dk/services/kvit/)

## The Latest Version


The latest version can be found at
https://bitbucket.org/genomicepidemiology/kvit/overview

## Documentation


The documentation available as of the date of this release can be found at
https://bitbucket.org/genomicepidemiology/kivt/overview.


License
=======

Copyright (c) 2014, Ole Lund, Technical University of Denmark
All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
